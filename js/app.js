define(
	["jquery","underscore","backbone", "races", "units", "items", "spells", "calc"],
	function($,_,Backbone, races, units, items, spells, Calc)
{
	var App = Backbone.View.extend({

		el: 'body',

		events: {
			"click #age-selector a" : "changeAge",
			"click #race-selector a": "changeRace",
			"click #export-build": "exportBuild",
			"click #import-build": "importBuild",
			"click #reset-build": "resetBuild",
			"change #pretender input[type=number]": "changePretenderStats",
			"change #dom-score input[type=number]": "changeDomScore",
			"click #pretender-timer a": "changeSleepTimer"
		},

		initialize: function(){

		    // Assign to the view
		    this.races = races;
		    this.spells = spells;
		    this.units = units;
		    this.items = items;
		    this.calc = new Calc;

		    this.current_races = [];
		    this.current_race = null;

		    // Cache views
		    this.$ageSelector = $('#age-selector');
		    this.$raceSelector = $('#race-selector');
		    this.$body = $('#body');
		    this.$footer = $('#footer');
		    this.$pretender = $('#pretender');
		    this.$domScore = $('#dom-score');

		    // Finish Up
		    this.render();
		    console.info('Dom4Calc Loaded successfully');
		    $('#app-loader').fadeOut();
		},

		changePretenderStats: function(e){

			var points = [];
			_.each(this.$pretender.find('input[type=number]'), function(ele){
				points.push( parseInt($(ele).val()) );
			}, this);
			this.calc.magic_stats = points;

			this.render();
		},

		changeDomScore: function(e){

			var points = [];
			_.each(this.$domScore.find('input[type=number]'), function(ele){
				points.push( parseInt($(ele).val()) );
			}, this);
			this.calc.dominion_stats = points;

			this.render();

		},

		// Called on age change, updates the current race working set
		changeAge: function(e){
			var $age = $(e.target).closest('a');
			if(!$age.length) return false;
			this.$ageSelector.prev().text($age.text());
			this.current_races = this.races.getByAge($age.text().toLowerCase());
			return this.render();
		},

		changeRace: function(e){
			var $race = $(e.target).closest('a');
			if(!$race.length) return false;
			this.$raceSelector.prev().text($race.text());
			this.current_race = this.races.where({name : $race.text()})[0];
			return this.render();
		},

		changeSleepTimer: function(e){
			var time = parseInt($(e.target).closest('a').data('time'));
			if( time === 0 ){ this.calc.prison_time = 0; }
			if( time === 1 ){ this.calc.prison_time = 1; }
			if( time === 2 ){ this.calc.prison_time = 2; }
			$('#pretender-timer a').each(function(){
				$(this).removeClass('btn-success').removeClass('btn-default');
				if( time === parseInt($(this).data('time')) ){
					$(this).addClass('btn-success');
				} else {
					$(this).addClass('btn-default');
				}
			});
			this.render();
		},

		exportBuild: function(){
			console.error('export not working');
		},

		importBuild: function(){

		},

		resetBuild: function(){
			if ( confirm("Are you sure you want to reset your build?") == false ){ return; }
			console.info("Build Reset");
		},

		// render the view
		// if force is true, override caching options
		render: function(force){

			$('#remaining span').text(this.calc.getPoints());

			// Navbar
			if(this.current_races.length > 0){
				this.$raceSelector.empty();
				this.current_races.each(function(race){
					this.$raceSelector.append("<li><a href='#'>"+race.get('name')+"</a></li>");
				}, this);
			}

			// Body
			if( this.current_race !== null ){
				this.$body.find('h1').html( this.current_race.get('name') + "<em>, "+this.current_race.get("epithet")+"</em>" );
			}

			// Spells
			var spell_html = "";
			this.spells.each(function(spell){
				spell_html += "<tr><td>"+spell.get('name')+"</td><td></td></tr>";
			}, this);
			//$('#active-spells tbody').empty().append(spell_html);

			// Footer
			this.$footer.find('#race-count').html(this.races.length);
			this.$footer.find('#unit-count').html(this.units.length);
			this.$footer.find('#item-count').html(this.items.length);
			this.$footer.find('#spell-count').html(this.spells.length);
		}
	});

	return new App;

});
