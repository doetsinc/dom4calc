define(["backbone"], function(Backbone){

	var Pretender = Backbone.Model.extend({});
	var Pretenders = Backbone.Collection.extend({
		model: Pretender
	});

	return new Pretenders;

});