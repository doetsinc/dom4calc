define(["underscore"],function(_){

	return function(){


		// 0,1,2
		this.prison_time = 0;

		this.magic_stats = [0,0,0,0,0,0,0,0];

		this.dominion_stats = [1,0,0,0,0,0,0];

		this.getPoints = function(){

			// stubs
			var points = 400; // ?
			var new_path_cost = 60;

			// Magic Calculation
			_.each(this.magic_stats, function(val, key){
				points -= parseInt(val) * new_path_cost;
			}, this);

			// Dominion Calculation
			_.each(this.dominion_stats, function(val, key){
				points -= parseInt(val) * 40;
			}, this);

			// Prison Calculations
			if(this.prison_time === 1){ points += 150; }
			if(this.prison_time === 2){ points += 250; }

			return points;

		};

		this.getHash = function(){

		};

		this.setHash = function(hash){

		};

	};

});