// Defines the races object
define(["backbone"],function(Backbone){

	// Race Model
	var Race = Backbone.Model.extend({

	});

	// Race Collection
	var Races = Backbone.Collection.extend({
		model: Race,

		getByAge: function(age){
			return new Races(this.filter(function(race){
				return race.get('age').substr(0,3) == age.substr(0,3);
			}));
		}
	});

	// Instances races
	return new Races([
		{id: 5,	name: "Arcoscephale",	epithet: "Golden Era",	filename:"Arc",	age:"early_arcoscephale"},
		{id: 7,	name: "Ulm",	epithet: "Enigma of Steel",	filename:"Ul",	age:"early_ulm"},
		{id: 9,	name: "Sauromatia",	epithet: "Amazon Queens",	filename:"Sa",	age:"early_sauromatia"},
		{id: 11, name: "Machaka",	epithet: "Lion Kings",	filename:"Mac",	age:"early_machaka"},
		{id: 13, name: "Abysia",	epithet: "Children of Flame",	filename:"Aby",	age:"early_abysia"},
		{id: 15, name: "C'tis",	epithet: "Lizard Kings",	filename:"Ct",	age:"early_ctis"},
		{id: 17, name: "Agartha",	epithet: "Pale Ones",	filename:"Ag",	age:"early_agartha"},
		{id: 19, name: "Fomoria",	epithet: "The Cursed Ones",	filename:"Fom",	age:"early_fomoria"},
		{id: 21, name: "Helheim",	epithet: "Dusk and Death",	filename:"He",	age:"early_helheim"},
		{id: 25, name: "Kailasa",	epithet: "Rise of the Ape Kings",	filename:"Ka",	age:"early_kailasa"},
		{id: 27, name: "Yomi",	epithet: "Oni Kings",	filename:"Yom",	age:"early_yomi"},
		{id: 29, name: "Ur",	epithet: "The First City",	filename:"Ur",	age:"early_ur"},
		{id: 31, name: "Xibalba",	epithet: "Vigil of the Sun",	filename:"Xi",	age:"early_xibalba"},
		{id: 34, name: "Ermor",	epithet: "Ashen Empire",	filename:"Ash",	age:"mid_ermor"},
		{id: 36, name: "Pythium",	epithet: "Emerald Empire",	filename:"Py",	age:"mid_pythium"},
		{id: 38, name: "Eriu",	epithet: "Last of the Tuatha",	filename:"Er",	age:"mid_eriu"},
		{id: 40, name: "Marignon",	epithet: "Fiery Justice",	filename:"Mar",	age:"mid_marignon"},
		{id: 42, name: "T'ien Ch'i",	epithet: "Imperial Bureaucracy",	filename:"Ti",	age:"mid_tienchi"},
		{id: 44, name: "Agartha",	epithet: "Golem Cult",	filename:"Ag",	age:"mid_agartha"},
		{id: 46, name: "Caelum",	epithet: "Reign of the Seraphim",	filename:"Ca",	age:"mid_caelum"},
		{id: 48, name: "Pangaea",	epithet: "Age of Bronze",	filename:"Pa",	age:"mid_pangaea"},
		{id: 50, name: "Vanheim",	epithet: "Arrival of Man",	filename:"Va",	age:"mid_vanheim"},
		{id: 52, name: "Vanarus",	epithet: "Land of the Chuds",	filename:"Rus",	age:"mid_rus"},
		{id: 54, name: "Shinuyama",	epithet: "Land of the Bakemono",	filename:"shi",	age:"mid_bakemono"},
		{id: 57, name: "Nazca",	epithet: "Kingdom of the Sun",	filename:"Naz",	age:"mid_nazca"},
		{id: 60, name: "Arcoscephale",	epithet: "Sibylline Guidance",	filename:"Arc",	age:"late_arcoscephale"},
		{id: 62, name: "Lemuria",	epithet: "Soul Gates",	filename:"Lem",	age:"late_lemur"},
		{id: 64, name: "Ulm",	epithet: "Black Forest",	filename:"BF",	age:"late_ulm"},
		{id: 66, name: "Mictlan",	epithet: "Blood and Rain",	filename:"Mi",	age:"late_mictlan"},
		{id: 69, name: "Jomon",	epithet: "Human Daimyos",	filename:"Jom",	age:"late_jomon"},
		{id: 71, name: "Abysia",	epithet: "Blood of Humans",	filename:"BoH",	age:"late_abysia"},
		{id: 73, name: "C'tis",	epithet: "Desert Tombs",	filename:"DT",	age:"late_ctis"},
		{id: 75, name: "Midgård",	epithet: "Age of Men",	filename:"Mid",	age:"late_midgard"},
		{id: 77, name: "Bogarus",	epithet: "Age of Heroes",	filename:"Bog",	age:"late_rus"},
		{id: 79, name: "Gath",	epithet: "Last of the Giants",	filename:"Ga",	age:"late_gath"},
		{id: 81, name: "Xibalba",	epithet: "Return of the Zotz",	filename:"Xi",	age:"late_xibalba"},
		{id: 84, name: "R'lyeh",	epithet: "Time of Aboleths",	filename:"Rl",	age:"early_rlyeh"},
		{id: 86, name: "Oceania",	epithet: "Coming of the Capricorns",	filename:"Oc",	age:"early_oceania"},
		{id: 88, name: "R'lyeh",	epithet: "Fallen Star",	filename:"Rl",	age:"mid_rlyeh"},
		{id: 90, name: "Oceania",	epithet: "Mermidons",	filename:"Oc",	age:"mid_oceania"},
		{id: 92, name: "R'lyeh",	epithet: "Dreamlands",	filename:"Rl",	age:"late_rlyeh"}
	]);

});