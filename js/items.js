define(["backbone","item_data"], function(Backbone,item_data){

	var Item = Backbone.Model.extend({});

	var Items = Backbone.Collection.extend({
		model: Item
	});

	return new Items(item_data);

});