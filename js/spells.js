define(["backbone","spell_data"], function(Backbone, spell_data){

	var Spell = Backbone.Model.extend({});

	var Spells = Backbone.Collection.extend({
		model: Spell
	});

	return new Spells(spell_data);

});