// Define the units
define( ["backbone", "unit_data"], function(Backbone,unit_data){

	var Unit = Backbone.Model.extend({

	});
	var Units = Backbone.Collection.extend({
		model: Unit
	});

	return new Units(unit_data);

});