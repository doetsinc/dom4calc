var _ = require('underscore');

module.exports = function(grunt){

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		uglify: {
			build: {
				src: 'js/<%= pkg.name %>.js',
				dest: 'build/<%= pkg.name %>.min.js'
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-uglify');

	//Default Task
	grunt.registerTask('default', ['uglify']);

	// data build
	grunt.registerTask('convert', "Convert information from data files", function(){

		var errors = {missing_desc: 0};
		var getTsv = function(csv){
			var raw = grunt.file.read('dom4inspector/gamedata/'+csv+'.csv');
			var lines = raw.match(/[^\r\n]+/g);
			var headers = lines[0].split("\t");
			var data = [];
			for(var i = 1; i < lines.length;i++ ){

				// Merge and clean
				var row = _.object( headers, lines[i].split("\t") );
				if(typeof row.test !== "undefined"){ delete row.test; }

				// Filter
				if(row.name == '...'){
					continue;
				}

				// Descriptions
				var file_name,file_path;
				if(csv == "spells"){
					file_name = row.name.replace(/ /g,'').replace('\'','');
					file_path = 'dom4inspector/gamedata/spelldescr/' + file_name + '.txt';
				}
				if(csv == "BaseI"){
					file_name = row.name.replace(/ /g,'').replace('\'','');
					file_path = 'dom4inspector/gamedata/itemdescr/' + file_name + '.txt';
				}
				if(csv == "BaseU"){
					file_name = row.id.substring(0, "0000".length - row.id.length) + row.id;
					file_path = "dom4inspector/gamedata/unitdescr/" + file_name + ".txt";
				}
				if(!file_path){ continue; }

				try {
					row.description = grunt.file.read(file_path);
				} catch (e) {
					errors.missing_desc += 1;
				}


				data.push(row);
			}
			return data;
		};

		var spell_data = getTsv("spells");
		var unit_data = getTsv("BaseU");
		var item_data = getTsv("BaseI");

		grunt.file.write('js/spell_data.js', "define(function(){return " + JSON.stringify(spell_data) + ";});" );
		grunt.file.write('js/unit_data.js', "define(function(){return " + JSON.stringify(unit_data) + ";});" );
		grunt.file.write('js/item_data.js', "define(function(){return " + JSON.stringify(item_data) + ";});" );

		// Convert Units



		// Finish Up
		console.log("Convert Complete");
		console.log("Total Spells: " + spell_data.length);
		console.log("Total Units: " + unit_data.length);
		console.log("Total Items: " + item_data.length);
		console.log("Missing descriptions: " + errors.missing_desc);


	});

};